package com.ca.ss.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ca.ss.entity.Student;

@RestController
@RequestMapping("management/api/v1/students")
public class StudentManagementController {



public final List<Student> students=Arrays.asList(new Student(1,"qwerty"),new Student(2,"ytrewq"),new Student(3,"asdfg"));	

@GetMapping
@PreAuthorize("hasAnyRole('ROLE_ADMIN,ROLE_ADMINTRAINEE')")
public List<Student> getStudents() {
	return students;
}
	
@PostMapping
@PreAuthorize("hasAuthority('student:write')")
public void addStudnet(@RequestBody Student std)
{
	System.out.println(std);
}

@DeleteMapping(path="{studentId}")
@PreAuthorize("hasAuthority('student:write')")
public void deleteStudent(@PathVariable("studentId") Integer id)
{
	System.out.println(id);
}

@PutMapping(path= "{studentId}")
@PreAuthorize("hasAuthority('student:write')")
public void updateStudent(@PathVariable("studentId") Integer id,@RequestBody Student student )
{
System.out.println(id+" updated value of the id"+student);	
}
}
