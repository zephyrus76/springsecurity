package com.ca.ss.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ca.ss.entity.Student;

@RestController
@RequestMapping("api")
public class StudentController {

	private static List<Student> STUDENTS=Arrays.asList(new Student(1,"sanjiv rai"),new Student(2,"rajat rai"));
	
	
	
	@GetMapping(path="/student/{studentId}")
	public Student getStudent(@PathVariable("studentId") Integer studentId)
	{
		return STUDENTS.stream().filter(student->studentId.equals(student.getStudentId())).findFirst().orElseThrow(()->new IllegalStateException("Student "+studentId+" does not exist"));
	}
}
