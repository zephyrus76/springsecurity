package com.ca.ss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCaSpringSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCaSpringSecurityApplication.class, args);
	}

}
