package com.ca.ss.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PasswordConfig {
 
	@Bean
	public PasswordEncoder passwordencoder()
	{
		//10 is the strength of the password
	return new BCryptPasswordEncoder(10);	
	}
}
