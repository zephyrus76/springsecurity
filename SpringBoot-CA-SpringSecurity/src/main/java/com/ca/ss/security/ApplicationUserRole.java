package com.ca.ss.security;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.google.common.collect.Sets; 
import  com.ca.ss.security.ApplicationUserPermission;
public enum ApplicationUserRole {
	  STUDENT(Sets.newHashSet()),
	    ADMIN(Sets.newHashSet(ApplicationUserPermission.COURSE_READ, ApplicationUserPermission.COURSE_WRITE, ApplicationUserPermission.STUDENT_READ
	    		, ApplicationUserPermission.STUDENT_WRITE)),
	    ADMINTRAINEE(Sets.newHashSet(ApplicationUserPermission.COURSE_READ, ApplicationUserPermission.STUDENT_READ));

	    private final Set<String> permissions;

	    ApplicationUserRole(HashSet<String> hashSet) {
	        this.permissions = hashSet;
	    }

	    public Set<String> getPermissions() {
	        return permissions;
	    }

	    public Set<SimpleGrantedAuthority> getGrantedAuthorities() {
	        Set<SimpleGrantedAuthority> permissions = getPermissions().stream()
	                .map(permission -> new SimpleGrantedAuthority(permission.toString()))
	                .collect(Collectors.toSet());
	        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
	        return permissions;
	    }

}